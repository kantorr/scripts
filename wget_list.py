

import os
import sys

# Add the current directory to path for imported modules 
CUR_DIR = os.path.dirname( os.path.abspath(__file__) )
sys.path.append( os.path.dirname(CUR_DIR) )

import wget

#------------------------------------------------------------------------------
def download_file(url, dst):
	if os.path.exists(dst):
		os.remove(dst)
	wget.download(url, dst, wget.progress_callback_simple)

#******************************************************************************
if __name__ == '__main__':
	import traceback
	try:
		strSource = sys.argv[1]

		with open(strSource, "r") as in_file:
			for line in in_file:
				if len(line) == 0:
					continue

				line = line.strip()
				if  line == "" or line.strip()[0] == '#':
					continue

				file_name = line.split('/')[-1]
				print "Trying to download: {}".format(line)
				download_file(line, file_name)
				# print "Done: {}".format(file_name)
	except:
		traceback.print_exc()
		input()

	finally:
		pass
