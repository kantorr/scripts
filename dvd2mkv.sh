# ffmpeg -i input.mp4 -vcodec copy -acodec pcm_s16le output.mov
#
# ffmpeg -fflags +genpts -analyzeduration 1000000k -probesize 1000000k -i movie.vob -c:v copy -c:a pcm_s16le -map 0:1 -map 0:2 movie.mkv

vob_file="$1"
mkv_file="$2"

#                                      1200 seconds  500 mega-bytes
input_flags="-fflags +genpts -analyzeduration 1200M -probesize 500M"
# Disable data-stream of dvd media: -dn 
# Map every track in stream 0: -map 0 
# Copy streams instead of transcoding (codec): -c copy
# Video encoder h264: -c:v h264
# CRF stands for Constant Rate Factor: -crf 18
# "The range of the CRF scale is 0–51, where 0 is lossless, 23 is the default, 
#  and 51 is worst quality possible. A lower value generally leads to higher quality, 
#  and a subjectively sane range is 17–28. Consider 17 or 18 to be visually lossless 
#  or nearly so."
# output_flags="-dn -map 0 -c copy -c:v h264 -crf 18"
output_flags="-dn -map 0 -c copy -c:v libx265 -crf 18"

echo "To copy all sreams use option: -map 0"
echo "To copy sreams starting from stream 0:1 use options: -map 0:1 -map 0:2 -map 0:3 -map 0:4 -map 0:5"
echo "To convert audio from: pcm_dvd use option: -c:a pcm_s16le"

read -p "Press [Enter] key to start process..."

ffmpeg_cmd="ffmpeg $input_flags -i $vob_file $output_flags "${@:3}" $mkv_file"
echo $ffmpeg_cmd
$ffmpeg_cmd
