#!/bin/bash
#
# safecopy directories and group of files
#
#

#------------------------------------------------------------------------------
function get_file_size {
	# return `stat -c%s "$1"`
	return `du -b "$1"`
}

#******************************************************************************
# echo Script name: $0
# echo $# arguments

if [ $# -le 1 ]; then
	echo "sfcp: Error: Illegal number of parameters"
	echo "sfcp: Please specify the source and destination for safely copy"
	exit
fi

src="$1"
dst="$2"

# echo "$src"
# echo "$dst"

stg1="> STAGE[1] <"
stg2=">> STAGE[2] <<"
stg3=">>> STAGE[3] <<<"
stg4=">>>> STAGE[4] <<<<"

outStage1=stage1.badblocks
outStage2=stage2.badblocks
outStage3=stage3.badblocks
outStage4=stage4.badblocks

#------------------------------------------------------------------------------
function clean_outs {
	rm -f $outStage1
	rm -f $outStage2
	rm -f $outStage3
	rm -f $outStage4
}

#------------------------------------------------------------------------------
function safe_copy_stage1 {
	echo "sfcp: $stg1 Skip 10% from the bad block"
	echo "sfcp: Rescue most of the data fast, using no retries and avoiding bad areas."
	# echo safecopy "$1" "$2" --stage1
	safecopy "$1" "$2" --stage1
	local status=$?
	echo "sfcp: $stg1: Ended with: $status"
	if [[ -s $outStage1 ]]; then
		echo "sfcp: $stg1: Found bad blocks"
	fi
}

#------------------------------------------------------------------------------
function safe_copy_stage2 {
	echo "sfcp: $stg2 Skip 128 blocks from the bad one"
	echo "sfcp: Rescue more data, using no retries but searching for exact ends of bad areas."
	# echo safecopy "$1" "$2" --stage2
	safecopy "$1" "$2" --stage2
	local status=$?
	echo "sfcp: $stg2: Ended with: $status"
	if [[ -s $outStage2 ]]; then
		echo "sfcp: $stg2: Found bad blocks"
	fi
}

#------------------------------------------------------------------------------
function safe_copy_stage3 {
	echo "sfcp: $stg3 Skip 4 blocks from the bad one"
	echo "Rescue more data, using low level access."
	# Very slow process !!!
	# safecopy "$1" "$2" --stage3
	# echo safecopy -f 4* -r 1* -R 2 -I $outStage2 -o $outStage3 --sync "$1" "$2"
	safecopy -f 4* -r 1* -R 2 -I $outStage2 -o $outStage3 --sync "$1" "$2"
	local status=$?
	echo "sfcp: $stg3: Ended with: $status"
	if [[ -s $outStage3 ]]; then
		echo "sfcp: $stg3: Found bad blocks"
	fi
}

#------------------------------------------------------------------------------
function safe_copy_stage4 {
	echo "sfcp: $stg4 Read each block"
	echo "Rescue everything that can be rescued, using low level access."
	# Very slow process !!!
	# echo safecopy -f 1* -r 1* -R 2 -I $outStage3 -o $outStage4 --sync "$1" "$2"
	safecopy -f 1* -r 1* -R 2 -I $outStage3 -o $outStage4 --sync "$1" "$2"
	local status=$?
	echo "sfcp: $stg4: Ended with: $status"
	if [[ -s $outStage4 ]]; then
		echo "sfcp: $stg4: Found bad blocks"
	fi
}

#------------------------------------------------------------------------------
function safe_copy_stages_1_3 {
	echo "sfcp: safe_copy_stages_1_3: ..."
	# echo "$@"
	local srcFile="$1"
	local dstFile="$2"

	# Enter Stage1
	safe_copy_stage1 "$srcFile" "$dstFile"
	if [ -s $outStage1 ]; then
		# Enter Stage2
		safe_copy_stage2 "$srcFile" "$dstFile"
		if [ -s $outStage2 ]; then
			# Enter Stage3
			safe_copy_stage3 "$srcFile" "$dstFile"
		fi
	fi
}

#------------------------------------------------------------------------------
function safe_copy_stages_1_4 {
	echo "sfcp: safe_copy_stages_1_4: ..."
	# echo "$@"
	local srcFile="$1"
	local dstFile="$2"

	# Enter Stage1
	safe_copy_stages_1_3 "$srcFile" "$dstFile"
	if [ -s $outStage3 ]; then
		# Move the destination, resolved from stage3
		mv "$dstFile" "$dstFile".stage3
		# Clean the previous output information about bad-blocks
		clean_outs
		# Do the stages from 1 till 3 once more, because the readed data still holded in the memory
		safe_copy_stages_1_3 "$srcFile" "$dstFile"
		if [ -s $outStage3 ]; then
			# Stiil have bad-blocks from stage3
			# Enter Stage4
			safe_copy_stage4 "$srcFile" "$dstFile"
		else
			# No bad-blocks found, file is recovered
			rm -f "$dstFile".stage3
		fi
	fi
}

#------------------------------------------------------------------------------
function safe_copy_file {
	echo "sfcp: safe_copy_file: ..."
	# echo "$@"
	local srcFile="$1"
	local dstFile="$2"

	# Enter Stage1
	safe_copy_stages_1_4 "$srcFile" "$dstFile"
	if [ -s $outStage4 ]; then
		# Remove the destination, resolved from stage3
		rm -f "$dstFile".stage3
		# Move the destination, resolved from stage4
		mv "$dstFile" "$dstFile".stage4
		# Save size of the previous stage3.badblocks output
		local sizeStage3Prev=`du -b "$outStage3"`
		# Do the stages from 1 till 3 once more, because the readed data still holded in the memory
		safe_copy_stages_1_3 "$srcFile" "$dstFile"
		local sizeStage3=`du -b "$outStage3"`
		# If have not empty stage3.badblocks (still have not resolved data) AND
		# its size differ from previous output (here was progression in resolving);
		# then try the next stage...
		if [[ -s $outStage3 ]] && [[ $sizeStage3Prev != $sizeStage3 ]]; then
			# Enter Stage4
			safe_copy_stage4 "$srcFile" "$dstFile"
			if [[ -s $outStage4 ]]; then
				# Rename the destination file
				mv "$dstFile" "$dstFile".BadBlocks
			fi
		else
			# File has been successfully recovered
			rm -f "$dstFile".stage4
		fi
	fi
}

#------------------------------------------------------------------------------
function do_file {
	local srcFile="$1"
	local dstFile="$2"

	safe_copy_file "$srcFile" "$dstFile"
	clean_outs
}

#------------------------------------------------------------------------------
function do_directory {
	echo "sfcp: do_directory"
	local srcDir="$1"
	local dstPath="$2"
	local dirName=$(basename "$srcDir")
	local dstDir="$dstPath"/"$dirName"
	# dirName2=${srcDir##*/}
	echo "SOURCE:      $srcDir"
	echo "DESTINATION: $dstDir"
	echo "sfcp: Making destination directory: $dstDir"
	mkdir -p "$dstDir"
	# echo "dirName2: $dirName2"
	for file in "$srcDir"/*; do
		# safecopy -f 4* "$file" "$dst"/"${file##*/}"
		# safecopy --stage3 "$file" "$dst"/"${file##*/}"
		echo "sfcp: ----------------------------------------------------------------"
		# do_file "$file" "$dstDir"/"${file##*/}"
		if [[ -d "$file" ]]; then
			echo "sfcp: Process Directory: ..."
			do_directory "$file" "$dstDir"
		elif [[ -f "$file" ]]; then
			#statements
			echo "sfcp: Process File: ..."
			# echo do_file "$file" "$dstDir"/"$(basename "$file")"
			do_file "$file" "$dstDir"/"$(basename "$file")"
		fi
	done
}

#******************************************************************************
if [[ -d "$src" ]]; then
	do_directory "$src" "$dst"
else
	do_file "$src" "$dst"/"$(basename "$src")"
fi

echo "sfcp: -->> JOB IS DONE <<--"

# TO DO
# 1. Strict resolving:
#    Continue to resolve till the output of stage3 (stage3.badblocks) is decreasing
#      while stage3.badblocks less_then stage3.badblocks.prev
#        do safe_copy_file
#
#  2. Make python script that will:
#     a. save in dictionary (internal DB) the full path and name of file
#        that was not recovered in 100%.
#     b. Do additional atempt to resolve those files.
#
