#!/bin/bash

args=$(getopt -l "brief:full" -o "b:f:h" -- "$@")

eval set -- "$args"

flgBrief=0
flgFull=0
flgHelp=0

while [ $# -ge 1 ]; do
        case "$1" in
                --)
                    # No more options left.
                    shift
                    break
                   ;;
                -b|--brief)
                    $flgBrief=1
                    shift
                    ;;
                -f|--full)
                    $flgFull=1
                    shift
                    ;;
                -h)
                    echo "Display some help"
                    exit 0
                    ;;
        esac

        shift
done

echo "flgBrief: $flgBrief"
echo "remaining args: $*"
