#!/bin/sh


# In this demonstration, I'll use sh -c 'echo first; false' (or true) for the first -exec. That will give some output and also have the selected exit code effect. Then echo second will be used for the second one. Assume that there's one file in the current directory.

# $ find . -type f -exec sh -c 'echo first; false' \; -exec echo second \;
# first
# $ find . -type f -exec sh -c 'echo first; true' \; -exec echo second \;
# first
# second
# $ find . -type f \( -exec sh -c 'echo first; false' \; -false -o -exec echo second \; \)
# first
# second
# $ find . -type f \( -exec sh -c 'echo first; false' \; -false -o -exec echo second \; \)
# first
# second
# A real command of this type would look like:

# find . -type f \( -exec command1 \; -false -o -exec command2 \; \)
# In the second set, the escaped parentheses group the two -exec clauses. The -false between them forces the test state to "false" and the -o causes the next expression (the second -exec) to be evaluated because of the -false.

# From man find:

# expr1 expr2
# Two expressions in a row are taken to be joined with an implied "and"; expr2 is not evaluated if expr1 is false.

# expr1 -a expr2
# Same as expr1 expr2.

# expr1 -o expr2
# Or; expr2 is not evaluated if expr1 is true.
# 
# --------------------------------------------------
# 
# $ for i in `find . -exec echo {} \;`; do cmd1 $i; cmd2 $i; done
# 

DS_STORE=".DS_Store"

if [ -z "$1" ]
then
	echo "Usage:"
	echo "reset-folder-cache <folder_name>"
	echo "    folder_name - must be set, it can be / for every folders"
else
	echo "Going to remove all \"$DS_STORE\" files from: \"$1\""
	sudo find "$1" -iname ".DS_Store" -exec echo {} \; -exec rm {} \;
fi
