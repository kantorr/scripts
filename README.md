Collection of several useful scripts for Linux, Mac OS X and Windows.

- reset-folder-cache - Reset direcories cache in Mac OS X.  
         Removes all ".DS_Store files from given directory and from all it's sub-directories.  

- sfcp - Safe copy directory.  
         Copies files recursively starting from given directory.  
         The copy is done by safecopy utility.  
