

import os
import sys

# Add the current directory to path for imported modules 
CUR_DIR = os.path.dirname( os.path.abspath(__file__) )
sys.path.append( os.path.dirname(CUR_DIR) )

import wget

#------------------------------------------------------------------------------
def files_from_playlist(filename):
	filePlayList = open(filename, "r")
	
	lstPlayItems = []
	for line in filePlayList.readlines():
		line = line.strip()
		if line[0] != '#':
			lstPlayItems.append(line)

	return lstPlayItems

#------------------------------------------------------------------------------
def download_file(url, dst):
	if os.path.exists(dst):
		os.remove(dst)
	wget.download(url, dst, wget.progress_callback_simple)

#******************************************************************************
if __name__ == '__main__':
	import traceback
	try:
		strSource = sys.argv[1]
		strPlayListFile = strSource.split('/')[-1]

		strFirstWord = strSource.split('/')[0].split(':')[0]
		if strFirstWord in "https":
			print "Downloading: %s" % strSource
			download_file(strSource, strPlayListFile)

			lstFiles = files_from_playlist(strPlayListFile)
			for i in xrange(len(lstFiles)):
				filename = lstFiles[i]
				# Process the chunk-list
				if "list" in filename.split('_')[0]:	# "list" in "chunklist"
					strURL = "/".join(strSource.split('/')[:-1]) + "/%s" % filename
					print "Downloading: %s" % strURL
					download_file(strURL, filename)

					lstChunks = files_from_playlist(filename)
					for j in xrange(len(lstChunks)):
						chunk = lstChunks[j]
						strURL = "/".join(strSource.split('/')[:-1]) + "/%s" % chunk
						print "Downloading: %s (%d/%d)" % (strURL, j + 1, len(lstChunks))
						download_file(strURL, chunk)
				else:
					strURL = "/".join(strSource.split('/')[:-1]) + "/%s" % filename
					print "Downloading: %s (%d/%d)" % (strURL, i + 1, len(lstFiles))
					download_file(strURL, filename)

	except:
		traceback.print_exc()
		input()

	finally:
		pass
